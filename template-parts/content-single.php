<?php
/**
 * The template part for displaying single posts
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="content-post-title">
	<h2><?php the_title(); ?></h2>
	 <p>By <a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ); ?>" rel="author" class="author"><?php echo get_author_name();?></a></p>
   </div>
    <hr>
     <div class="content-post-all-meta">
          <ul>
            <li><i class="fa fa-clock-o"></i><?php echo get_the_date(); ?></li>
            <li><a href="#comments" class="scroll-d"><i class="fa fa-comment fa-flip-horizontal"></i><?php comments_number( 'No Comments', 'one response', '% responses' ); ?></a></li>
          </ul>
        </div>
  <?php if (has_post_thumbnail( $post->ID ) ): ?>
  <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
    <div class="content-post-img"> <img src="<?php echo $image[0]; ?>" alt="<?php the_title(); ?>" class="img-responsive"> </div>

<?php endif; ?>
<div class="content-post-desc">
	<?php holdfolio_excerpt(); ?>
</div>


	<div class="entry-content">
		<?php
			the_content();

			wp_link_pages( array(
				'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'holdfolio' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
				'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'holdfolio' ) . ' </span>%',
				'separator'   => '<span class="screen-reader-text">, </span>',
			) );

			if ( '' !== get_the_author_meta( 'description' ) ) {
				get_template_part( 'template-parts/biography' );
			}
		?>
	</div><!-- .entry-content -->
<div class="clearfix mrgnT30">
						<hr class="blue_hr clearfix">
					</div>
	<div class="entry-footer">
		<?php //holdfolio_entry_meta(); ?>
		<?php
			edit_post_link(
				sprintf(
					/* translators: %s: Name of current post */
					__( 'Edit<span class="screen-reader-text"> "%s"</span>', 'holdfolio' ),
					get_the_title()
				),
				'<span class="edit-link">',
				'</span>'
			);
		?>
	</div><!-- .entry-footer -->
</article><!-- #post-## -->
