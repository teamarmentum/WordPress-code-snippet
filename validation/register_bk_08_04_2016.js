// JavaScript Document
// JavaScript Document
// JavaScript Document
// Signup
var Signup = function() 
{
	"use strict";
	// Handle Signup Form
	var handleSignupForm = function() {
	$().ready(function() {
	// validate the comment form when it is submitted
	var validator =$("#regiserForm").validate({
	rules: {
			fname: {required: true},
			lname: {required: true},
			username: {required: true},
			email: {required: true, email:true},
			phone: {required: true},
			password: {required: true},
			confirm_password: 
			{
				 required: true,	
				 equalTo: "#password"
			},
			invest:{required: true},
			remember:{required: true}
		},
		messages: 
		{
		fname:{required: "Please Enter First Name"},
		lname:{required: "Please Enter Last Name"},
		username: {required: "Please Enter Username"},
		email:{required: "Please Enter Email",email:"Please Enter Valid Email"},
		password: {required: "Please Enter Password"},
		confirm_password: {required: "Please Confirm Password"},
		invest: {required: "Please Enter Investment Amount"},
		remember: {required: "Accept Our Terms And conditions "},
        }
   });
   
});
}
			
				return {
					init: function() {
						handleSignupForm(); // initial setup for comment form
					}
				}
			}();
				
				$(document).ready(function() {
   				 Signup.init();
				});