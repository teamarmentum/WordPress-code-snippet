<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Holdfolio
 * @since Holdfolio 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
    
    <link rel="icon" href="<?php echo get_template_directory_uri();?>/images/favicon.ico" type="image/x-icon"/>

    <!-- Bootstrap -->
<link href="<?php echo get_template_directory_uri();?>/stylesheets/styles.css" rel="stylesheet" type="text/css" />
<link href="<?php echo get_template_directory_uri();?>/stylesheets/custom.css" rel="stylesheet" type="text/css" />


    <!-- Libs CSS -->
    <link href="<?php echo get_template_directory_uri();?>/stylesheets/lib/font-awesome.min.css" rel="stylesheet"> 
    <link href="<?php echo get_template_directory_uri();?>/stylesheets/lib/flexslider.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri();?>/stylesheets/lib/prettyPhoto.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri();?>/stylesheets/lib/owl.carousel.css" rel="stylesheet"> 
    
    <!-- On Scroll Animations -->
    <link href="<?php echo get_template_directory_uri();?>/stylesheets/lib/animate.min.css" rel="stylesheet">
<link href="<?php echo get_stylesheet_directory_uri(); ?>/fonts/gotham_font.css" rel="stylesheet">


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    
	<?php wp_head(); ?>
<link href="<?php echo get_template_directory_uri();?>/new_style.css" rel="stylesheet" type="text/css" />
<link href="<?php echo get_template_directory_uri();?>/new_style_responsive.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo get_template_directory_uri();?>/js/custom.js"></script>
</head>
<?php 
if($post->post_name=='contact-us' || $post->post_name=='register' || $post->post_name=='login' || $post->post_name=='profile' || $post->post_name=='edit-profile' || $post->post_name=='profile-info' || $post->post_name=='investment-steps' ){?>
<body <?php body_class(); ?> id="contact_page">
<?php }else{ ?>

<body <?php body_class(); ?>>
<?php } ?>
<div id="page" <?php  echo $user_ID;?>>

<!-- Fixed navbar -->
    <nav class="navbar navbar-default navbar-fixed-top">
     
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
             <?php holdfolio_the_custom_logo(); ?>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
           <?php 
	   	 $loggedin_user_id = get_current_user_id();
 		 if($loggedin_user_id){
		 $current_user_id= $loggedin_user_id ;
		 $args=array("class"=>"img-circle");
		 $alt="";
		 $default="";
		 $user_data=get_userdata( $current_user_id );
		  //$img=get_avatar_url( $current_user_id, 37, $default, $alt, $args );
		 ?> 
           <?php
									wp_nav_menu( array(
										'menu' => 'Login user menu',
										'container' => '',
										'container_id' => '',
										'container_class' => '',
										'container_id' => '',
										'menu_class' => 'nav navbar-nav',
										'menu_id' => '',
										'echo' => true,
										'fallback_cb' => 'wp_page_menu',
										'before' => '',
										'after' => '',
										'link_before' => '',
										'link_after' => '',
										'depth' => 0,
										'walker' => '',
										'theme_location' => ''
									 ) );
								?>
        <ul class="nav navbar-nav navbar-right">
             <li class="user"><a href="<?php echo get_site_url();?>/profile/profile-info/" >Hi, <?php echo $user_data->user_firstname;?>
         <img src="<?php echo user_img_url($current_user_id); ?>" style="width:37px;" class="img-circle"/>
				 <?php /*<i class="fa fa-caret-down" aria-hidden="true"></i>*/ ?></a>
                <?php /*<ul class="dropdown-menu user_dd">
                  <li><a href="<?php echo get_site_url();?>/dashboard/">Dashborad</a></li>
                  <li><a href="<?php echo get_site_url();?>/profile/profile-info/">Profile Detail</a></li>
                </ul> */ ?>
              
    
                
			</li>
            <li class="signup"><a href="<?php echo wp_logout_url(home_url('/login/?act=logout')); ?>">Logout</a></li>
          </ul>
        <?php }else{ ?>
        
        
           <?php
									wp_nav_menu( array(
										'menu' => '',
										'container' => '',
										'container_id' => '',
										'container_class' => '',
										'container_id' => '',
										'menu_class' => 'nav navbar-nav',
										'menu_id' => '',
										'echo' => true,
										'fallback_cb' => 'wp_page_menu',
										'before' => '',
										'after' => '',
										'link_before' => '',
										'link_after' => '',
										'depth' => 0,
										'walker' => '',
										'theme_location' => 'primary'
									 ) );
								?>
        <ul class="nav navbar-nav navbar-right">
            <li class="login"><a href="<?php echo get_site_url();?>/login">Login</a></li>
            <li class="signup"><a href="<?php echo get_site_url();?>/register">Sign up <span class="sr-only">(current)</span></a></li>
          </ul>
        <?php } ?>
          
        
          
        </div><!--/.nav-collapse -->
     
    </nav>
<!-- end Nav -->




	

