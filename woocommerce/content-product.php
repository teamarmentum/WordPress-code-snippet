<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.6.1
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
global $product;

// Ensure visibility
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}
?>





<div <?php post_class(); ?>>

<div class="image_block investment_list_view">
<div class="row">
							<div class="col-md-6 col-sm-5 col-xs-12">
								<img src="images/investment/image-03.jpg" alt="" class="img-big-width">
							</div>
							
							<div class="col-md-6 col-sm-7 col-xs-12 image_blk_r">
								<div class="investment_content image_blk_content">
									<div class="invest_title_group">
										<h3>Holdfolio Indiana V LLC</h3>
										<p>LA, Bay Area, San Deigo and Poland</p>
									</div>
									<!-- // end invest_title_group -->
									
									<div class="invest_brif">
										<div class="row">
											<div class="col-md-5 col-sm-5 col-xs-12 brder_r">
												<label>Cash on Cash Return</label>
												<span class="label_value"><?php echo $product->get_attribute( 'ccr_from' ); ?>% - <?php echo $product->get_attribute( 'ccr_to' ); ?>%</span>
											</div>
											<div class="col-md-4 col-sm-4 col-xs-12 brder_r">
												<label>Min. Investment</label>
												<span class="label_value">$2000</span>
											</div>
											<div class="col-md-3 col-sm-3 col-xs-12 brder_r">
												<label>IRR</label>
												<span class="label_value">14% - 18%</span>
											</div>
										</div>
										<!--// end row -->
									</div>
									<!-- end invest_brif -->
									
									<div class="invest_details">
									
										<p>No. of Properties  ...................................................... 10</p>
										<p>Property Type  ...................................................... SFR</p>
										<p>
											<a href="portfolio-page.html" class="btn btn-readmore">More Details </a>
										</p>
									</div>
									<!-- end invest_details -->
									
								</div>
							</div>
						</div>
	<?php
	/**
	 * woocommerce_before_shop_loop_item hook.
	 *
	 * @hooked woocommerce_template_loop_product_link_open - 10
	 */
	do_action( 'woocommerce_before_shop_loop_item' );

	/**
	 * woocommerce_before_shop_loop_item_title hook.
	 *
	 * @hooked woocommerce_show_product_loop_sale_flash - 10
	 * @hooked woocommerce_template_loop_product_thumbnail - 10
	 */
	do_action( 'woocommerce_before_shop_loop_item_title' );

	/**
	 * woocommerce_shop_loop_item_title hook.
	 *
	 * @hooked woocommerce_template_loop_product_title - 10
	 */
	do_action( 'woocommerce_shop_loop_item_title' );

	/**
	 * woocommerce_after_shop_loop_item_title hook.
	 *
	 * @hooked woocommerce_template_loop_rating - 5
	 * @hooked woocommerce_template_loop_price - 10
	 */
	do_action( 'woocommerce_after_shop_loop_item_title' );

	/**
	 * woocommerce_after_shop_loop_item hook.
	 *
	 * @hooked woocommerce_template_loop_product_link_close - 5
	 * @hooked woocommerce_template_loop_add_to_cart - 10
	 */
	do_action( 'woocommerce_after_shop_loop_item' );
	?>
    </div>

</div>
