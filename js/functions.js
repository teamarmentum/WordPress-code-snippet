/* global screenReaderText */
/**
 * Theme functions file.
 *
 * Contains handlers for navigation and widget area.
 */
$(document).ready(function() {
	$('a.scroll-d').click(function(){
    $('html, body').animate({
        scrollTop: $( $.attr(this, 'href') ).offset().top 
    }, 800);
    return false;
});
    
 	$('a.scoll-smooth').click(function(){
    $('html, body').animate({
        scrollTop: $( $.attr(this, 'href') ).offset().top-70
    }, 800);
    return false;
});   
    
    
});	
( function( $ ) {
	var body, masthead, menuToggle, siteNavigation, socialNavigation, siteHeaderMenu, resizeTimer;

	function initMainNavigation( container ) {

		// Add dropdown toggle that displays child menu items.
		var dropdownToggle = $( '<button />', {
			'class': 'dropdown-toggle',
			'aria-expanded': false
		} ).append( $( '<span />', {
			'class': 'screen-reader-text',
			text: screenReaderText.expand
		} ) );

		container.find( '.menu-item-has-children > a' ).after( dropdownToggle );

		// Toggle buttons and submenu items with active children menu items.
		container.find( '.current-menu-ancestor > button' ).addClass( 'toggled-on' );
		container.find( '.current-menu-ancestor > .sub-menu' ).addClass( 'toggled-on' );

		// Add menu items with submenus to aria-haspopup="true".
		container.find( '.menu-item-has-children' ).attr( 'aria-haspopup', 'true' );

		container.find( '.dropdown-toggle' ).click( function( e ) {
			var _this            = $( this ),
				screenReaderSpan = _this.find( '.screen-reader-text' );

			e.preventDefault();
			_this.toggleClass( 'toggled-on' );
			_this.next( '.children, .sub-menu' ).toggleClass( 'toggled-on' );

			// jscs:disable
			_this.attr( 'aria-expanded', _this.attr( 'aria-expanded' ) === 'false' ? 'true' : 'false' );
			// jscs:enable
			screenReaderSpan.text( screenReaderSpan.text() === screenReaderText.expand ? screenReaderText.collapse : screenReaderText.expand );
		} );
	}
	initMainNavigation( $( '.main-navigation' ) );

	masthead         = $( '#masthead' );
	menuToggle       = masthead.find( '#menu-toggle' );
	siteHeaderMenu   = masthead.find( '#site-header-menu' );
	siteNavigation   = masthead.find( '#site-navigation' );
	socialNavigation = masthead.find( '#social-navigation' );

	// Enable menuToggle.
	( function() {

		// Return early if menuToggle is missing.
		if ( ! menuToggle.length ) {
			return;
		}

		// Add an initial values for the attribute.
		menuToggle.add( siteNavigation ).add( socialNavigation ).attr( 'aria-expanded', 'false' );

		menuToggle.on( 'click.twentysixteen', function() {
			$( this ).add( siteHeaderMenu ).toggleClass( 'toggled-on' );

			// jscs:disable
			$( this ).add( siteNavigation ).add( socialNavigation ).attr( 'aria-expanded', $( this ).add( siteNavigation ).add( socialNavigation ).attr( 'aria-expanded' ) === 'false' ? 'true' : 'false' );
			// jscs:enable
		} );
	} )();

	// Fix sub-menus for touch devices and better focus for hidden submenu items for accessibility.
	( function() {
		if ( ! siteNavigation.length || ! siteNavigation.children().length ) {
			return;
		}

		// Toggle `focus` class to allow submenu access on tablets.
		function toggleFocusClassTouchScreen() {
			if ( window.innerWidth >= 910 ) {
				$( document.body ).on( 'touchstart.twentysixteen', function( e ) {
					if ( ! $( e.target ).closest( '.main-navigation li' ).length ) {
						$( '.main-navigation li' ).removeClass( 'focus' );
					}
				} );
				siteNavigation.find( '.menu-item-has-children > a' ).on( 'touchstart.twentysixteen', function( e ) {
					var el = $( this ).parent( 'li' );

					if ( ! el.hasClass( 'focus' ) ) {
						e.preventDefault();
						el.toggleClass( 'focus' );
						el.siblings( '.focus' ).removeClass( 'focus' );
					}
				} );
			} else {
				siteNavigation.find( '.menu-item-has-children > a' ).unbind( 'touchstart.twentysixteen' );
			}
		}

		if ( 'ontouchstart' in window ) {
			$( window ).on( 'resize.twentysixteen', toggleFocusClassTouchScreen );
			toggleFocusClassTouchScreen();
		}

		siteNavigation.find( 'a' ).on( 'focus.twentysixteen blur.twentysixteen', function() {
			$( this ).parents( '.menu-item' ).toggleClass( 'focus' );
		} );
	} )();

	// Add the default ARIA attributes for the menu toggle and the navigations.
	function onResizeARIA() {
		if ( window.innerWidth < 910 ) {
			if ( menuToggle.hasClass( 'toggled-on' ) ) {
				menuToggle.attr( 'aria-expanded', 'true' );
			} else {
				menuToggle.attr( 'aria-expanded', 'false' );
			}

			if ( siteHeaderMenu.hasClass( 'toggled-on' ) ) {
				siteNavigation.attr( 'aria-expanded', 'true' );
				socialNavigation.attr( 'aria-expanded', 'true' );
			} else {
				siteNavigation.attr( 'aria-expanded', 'false' );
				socialNavigation.attr( 'aria-expanded', 'false' );
			}

			menuToggle.attr( 'aria-controls', 'site-navigation social-navigation' );
		} else {
			menuToggle.removeAttr( 'aria-expanded' );
			siteNavigation.removeAttr( 'aria-expanded' );
			socialNavigation.removeAttr( 'aria-expanded' );
			menuToggle.removeAttr( 'aria-controls' );
		}
	}

	// Add 'below-entry-meta' class to elements.
	function belowEntryMetaClass( param ) {
		if ( body.hasClass( 'page' ) || body.hasClass( 'search' ) || body.hasClass( 'single-attachment' ) || body.hasClass( 'error404' ) ) {
			return;
		}

		$( '.entry-content' ).find( param ).each( function() {
			var element              = $( this ),
				elementPos           = element.offset(),
				elementPosTop        = elementPos.top,
				entryFooter          = element.closest( 'article' ).find( '.entry-footer' ),
				entryFooterPos       = entryFooter.offset(),
				entryFooterPosBottom = entryFooterPos.top + ( entryFooter.height() + 28 ),
				caption              = element.closest( 'figure' ),
				newImg;

			// Add 'below-entry-meta' to elements below the entry meta.
			if ( elementPosTop > entryFooterPosBottom ) {

				// Check if full-size images and captions are larger than or equal to 840px.
				if ( 'img.size-full' === param ) {

					// Create an image to find native image width of resized images (i.e. max-width: 100%).
					newImg = new Image();
					newImg.src = element.attr( 'src' );

					$( newImg ).on( 'load.twentysixteen', function() {
						if ( newImg.width >= 840  ) {
							element.addClass( 'below-entry-meta' );

							if ( caption.hasClass( 'wp-caption' ) ) {
								caption.addClass( 'below-entry-meta' );
								caption.removeAttr( 'style' );
							}
						}
					} );
				} else {
					element.addClass( 'below-entry-meta' );
				}
			} else {
				element.removeClass( 'below-entry-meta' );
				caption.removeClass( 'below-entry-meta' );
			}
		} );
	}

	$( document ).ready( function() {
		body = $( document.body );

		$( window )
			.on( 'load.twentysixteen', onResizeARIA )
			.on( 'resize.twentysixteen', function() {
				clearTimeout( resizeTimer );
				resizeTimer = setTimeout( function() {
					belowEntryMetaClass( 'img.size-full' );
					belowEntryMetaClass( 'blockquote.alignleft, blockquote.alignright' );
				}, 300 );
				onResizeARIA();
			} );

		belowEntryMetaClass( 'img.size-full' );
		belowEntryMetaClass( 'blockquote.alignleft, blockquote.alignright' );
	} );
} )( jQuery );


$(function () { 
  $('[data-toggle="tooltip"]').tooltip({trigger: 'manual'}).tooltip('show');
});  

// $( window ).scroll(function() {   
 // if($( window ).scrollTop() > 10){  // scroll down abit and get the action   
  $(".progress-bar").each(function(){
    each_bar_width = $(this).attr('aria-valuenow');
    $(this).width(each_bar_width + '%');
  });
       
 //  }  
// });

function invest(btn,pid,serv_root)
{
		$("#"+btn).html("Processing..");
		$("#"+btn).attr("disabled","disabled");
		
		var ajaxurl = serv_root+'/wp-admin/admin-ajax.php';
		jQuery.ajax({
		type:"POST",
		url: ajaxurl,
		dataType:'json',		
		data: {"action": "loadInvestment",'pid':pid},
		success:function(data)
		{
		if(data.result=='ok')
		{
			console.log(data.redirect);
			window.location.href=data.redirect;
		}
		else
		{
			console.log(data.message);
		}
		$("#"+btn).html("Invest");
		$("#"+btn).removeAttr("disabled","disabled");
		
		//console.log(data);
		return false;
		 
		}
		});
}

		function next(serv_root,s1,s2)
		{
			var err=[];
			$( ".download_docs li" ).each(function( index ) {
				
				if($( this ).find('a').attr("data-downloaded")=='No')
				{
					err.push('1');
				}
				
			});
			if(err.length>0)
			{
				$("#step1_response").html("<p class=\"err-step\">Please read all files.</p>");

			}
			else
			{
								var ajaxurl = serv_root+'/wp-admin/admin-ajax.php';
					jQuery.ajax({
					type:"POST",
					url: ajaxurl,
					dataType:'json',		
					data: {"action": "loadNext",'s1':s1,'s2':s2},
					success:function(data)
					{
					//window.location.reload();
					window.location = serv_root+'/investment-steps?step=2';
					return false;
		 
		}
		});
			}
			
		}
		
		
		
		jQuery('.list_n.download_docs li a').click(function(){
			var th = jQuery(this);
			var serv_root = th.attr('data-site-url');
			var sr = th.attr('data-download-count');
			var file = th.attr('data-download-file');
			var file_name = th.attr('data-dwonload-name');
			download_step_file(serv_root,sr,file,file_name);
		});
		function download_step_file(serv_root,sr,file,file_name)
		{
			var sr = parseInt(sr);
			
			var count = jQuery('.list_n.download_docs li').length;
			var next_btn ="";
			var prev_btn ="";
			if((count-1) > sr){
			next_btn = "<span class='next_pdf_lnk'>Next</span>";
				
			}
			if(sr !=0){
			prev_btn = "<span class='prev_pdf_lnk'>Previous</span>";
				
			}
			jQuery('.next_pdf_lnk')
				var ajaxurl = serv_root+'/wp-admin/admin-ajax.php';
					jQuery.ajax({
					type:"POST",
					url: ajaxurl,
					data: {"action": "downloadFile",'sr':sr,'file':file,'file_name':file_name},
					success:function(data)
					{
						
					var object = prev_btn+'<object data="'+file+'" type="application/pdf" width="100%" height=\"700px\"></object>'+next_btn;	
					$("#myModalViewPDF .modal-content").html(object);
					
					jQuery('.next_pdf_lnk').click(function(){
						var next_lnk = jQuery('.list_n.download_docs li:eq('+(sr+1)+') a');
							next_lnk.click();
					});
					jQuery('.prev_pdf_lnk').click(function(){
						var next_lnk = jQuery('.list_n.download_docs li:eq('+(sr-1)+') a');
							next_lnk.click();
					});
					$("#myModalViewPDF").modal('show');	
						
					$(".file_"+sr +" a").attr("data-downloaded","Yes");
					
					$(".file_"+sr +" a").addClass("dowloaded");
					//console.log(data);
						
					//window.location.href=data;
					return false;
					}
					});
			
			
		}
		
		$(document).ready(function() 
		{
			
			$("#inv_as").on('change',function()
			{
				if($(this).val()=='An LLC, Corporation, or Partnership' || $(this).val()=='A Trust')
				{
					$("#invas_box").show();
			if($(this).val()=='An LLC, Corporation, or Partnership')
			{
				$("#invas_box #txt").text('Name of LLC/Corp/Partnership');
				$("#name_inv_as").attr('placeholder','Name of LLC/Corp/Partnership');
				$("#name_inv_as").attr('title','Name of LLC/Corp/Partnership Required');
				$("#name_inv_as").show();

			}
			else
			{
				$("#invas_box #txt").text('Name of Trust');
				$("#name_inv_as").attr('placeholder','Name of Trust');
				$("#name_inv_as").attr('title','Name of Trust Required');
				$("#name_inv_as").show();
			}
			}
			else
			{
				$("#invas_box").hide();
				$("#name_inv_as").hide();

			}
			});
            
        });
		
		
		$(function () {
            $(".load_ppopup").click(function () {
				var fileName=$(this).data("file");
				var title=$(this).data("title");
			 var object = '<object data="'+fileName+'" type="application/pdf" width="100%" height=\"700px\"></object>';
				$("#myModal .modal-content").html(object);
				
				
            });
			
            $(".popup_file").click(function()
			{
				var fileName=$(this).data("file");
				
			 var object = '<object data="'+fileName+'" type="application/pdf" width="100%" height=\"700px\"></object>';
				$("#myModal .modal-content").html(object);
				$("#myModal").modal('show');
			});
			
            $("#reservspot").click(function() 
			{
					$("#myModalReserv").modal('show');	
			});
			
			
			
        });
		
		