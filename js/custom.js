jQuery(document).ready(function(){
    jQuery('.invest_details .btn.btn-readmore.reserve_spot').click(function(ev){
        ev.preventDefault();
    });
    function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
    }
jQuery('.calculate_investment,.form-invest_amount input').keypress(function(event){
    return isNumber(event)
});

jQuery('form.edit-form').submit(function(ev){
    var ili = jQuery('.form-invest_amount input');
    var ili_amount_p = ili.attr('placeholder');
    ili_amount = ili_amount_p.replace('Less than $','');
    ili_amountaa = ili_amount.replace(',','');
    var ili_amounta = parseInt(ili_amountaa);
    var ili_amountb = parseInt(ili.val());
    
				ili.removeClass('invalid_field');
				jQuery('.invalid_field_error_message').remove();
    if(ili_amountb > ili_amounta){
        ev.preventDefault();
		ili.addClass('invalid_field').focus();
		ili.after('<div class="invalid_field_error_message">Please enter value '+ili_amount_p+'</div>');
    }
});
     jQuery('.calculate_investment').keyup(function(){
              var th = jQuery(this);
              var closests = th.parent().parent().parent().parent();
              var amount_a = th.val();
              
              var ccr_a = th.attr('data-ccr-value');
              var amount = parseFloat(amount_a).toFixed(2);
              var ccr = parseFloat(ccr_a).toFixed(2);
              var total = (amount*ccr)/4;
             closests.find('.total_estimated_value_html').html(total);
              closests.find('.total_estimated_value').val(total);
            });
            
            
            jQuery('.validate_minimum_amount').click(function(ev){
                var th = jQuery(this);
                var min_amount = th.attr('data-min-amount');
              var closests = th.parent().parent();
              var current_amount = closests.find('.calculate_investment').val();
              current_amount2 = parseInt(current_amount);
              min_amount2 = parseInt(min_amount);
              
                    jQuery('#inv_amount-error2').remove();
                    jQuery('#inv_amount-error').remove();
                if(closests.find('.calculate_investment').val() ==''){
                 
                    closests.find('.calculate_investment').after('<label id="inv_amount-error" class="error" for="inv_amount">Please Enter Investment Amount</label>');   
                ev.preventDefault();
                return false
                    
                }
                if(current_amount2 < min_amount2){
                    closests.find('.calculate_investment').after('<label id="inv_amount-error2" class="error" for="inv_amount">Please Enter Amount Greater Than Minimum Investment</label>');
                ev.preventDefault();
                return false
                }
            });
            
            jQuery('[name="your-message"],[name="name_inv_as"],#user_login,.bright input[type="text"]').change(function(){
                var thval =  jQuery(this).val();
                 jQuery(this).val(thval.trim())
            });
            
            jQuery('body.woocommerce-lost-password .border-title span').html('Reset Password');
            
            
            jQuery('.show_order_id').click(function(ev){
                ev.preventDefault();
                var id = jQuery(this).attr('data-id');
                jQuery('#'+id).show();
            });
            jQuery('.close_popup_table_wrap').click(function(ev){
                ev.preventDefault();
                jQuery('.popup_table_wrap').hide();
            });
            
            
            
            
});