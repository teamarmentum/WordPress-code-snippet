<?php 
//Template Name: Home Page Template
get_header();
?>
<?php //dynamic_sidebar( 'home-search' ); ?>

<div class="jumbotron homepage">
      <div class="container">
        <h3>Invest in a portfolio of properties we already own.</h3>
        <div class="search_blk">
            <form name="searchemail" id="searchemail" method="post" action="<?php echo site_url('/register'); ?>">
            	<input type="text" name="email" id="email" placeholder="Enter your email address" />
                <input type="submit" value="View Investments"   />
            </form>
        </div>
        <!-- // search_blk  -->
        
        <div class="how_it_works">
        	
            	<img src="wp-content/themes/holdfolio/images/our-advantage.png" alt="How it works" />
        	
        </div> 
        <!-- // how_it_works  -->
      </div>
    </div>
    <!-- end jumbotron -->

<div class="container">
  <?php /*
if ( have_posts() ) :
while ( have_posts() ) : the_post();
 	the_content(); 
endwhile;
endif;  */
 ?>    <div class="row content-block">		<div class="col-md-3 col-sm-6 col-xs-12">			<h4 >  <?php the_field('first_column_title'); ?></h4>			<?php the_field('first_column_content'); ?>		</div>				<div class="col-md-3 col-sm-6 col-xs-12">			<h4 >  <?php the_field('second_column_title'); ?></h4>			<?php the_field('second_column_content'); ?>		</div>				<div class="col-md-3 col-sm-6 col-xs-12">			<h4 >  <?php the_field('third_column_title'); ?></h4>			<?php the_field('third_column_content'); ?>		</div>				<div class="col-md-3 col-sm-6 col-xs-12">			<h4 >  <?php the_field('fourth_column_title'); ?></h4>			<?php the_field('fourth_column_content'); ?>		</div></div><div class="client-testimonials">	<h2 class="text-center title_brdr">Here's what our clients have to say:</h2>	<?php echo do_shortcode('[xl_testimonial]'); ?></div><!-- end client-testimonials -->
</div>

<div class="row"><div class="container">
    <div class="col-md-6 widget_a_1">
        <h2>REDUCE YOUR RISK</h2>
        <p>By investing in a <b>diverse</b> portfolio of properties, you <b>decrease your risk and increase your return.</b></p>
    </div>
    <div class="col-md-6 widget_a_1">
        <h2>SIMPLIFY HOW YOU INVEST</h2>
        <p>Holdfolio is owned by <b>experienced & successful</b> full time real estate investors. Doing your due diligence has never been easier. You invest in properties we <b>already own.</b></p>
        
    </div>
    
</div></div>

<?php get_footer(); ?>
 
  <script src="//ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
  
  <!-- jQuery Form Validation code -->
  <script>
  
  // When the browser is ready...
  $(function() {
  
    // Setup form validation on the #register-form element
    $("#searchemail").validate({
    
        // Specify the validation rules
        rules: {
         
            email: {
                required: true,
                email: true
            },
          
        },
        
        // Specify the validation error messages
        messages: {
            	email:{required: "Please Enter Email",email:"Please Enter Valid Email"},
        },
        submitHandler: function(form) {
            form.submit();
        }
    });

  });
  
  </script>