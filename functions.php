<?php
/**
 * Twenty Sixteen functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @link https://codex.wordpress.org/Child_Themes
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * {@link https://codex.wordpress.org/Plugin_API}
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
include('inc/custom_functions.php');
/**
 * Twenty Sixteen only works in WordPress 4.4 or later.
 */
if ( version_compare( $GLOBALS['wp_version'], '4.4-alpha', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
}

if ( ! function_exists( 'holdfolio_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 *
 * Create your own holdfolio_setup() function to override in a child theme.
 *
 * @since Twenty Sixteen 1.0
 */
function holdfolio_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Twenty Sixteen, use a find and replace
	 * to change 'holdfolio' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'holdfolio', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for custom logo.
	 *
	 *  @since Twenty Sixteen 1.2
	 */
	add_theme_support( 'custom-logo', array(
		'height'      => 60,
		'width'       => 182,
		'flex-height' => true,
	) );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 1200, 9999 );

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'holdfolio' ),
		'social'  => __( 'Social Links Menu', 'holdfolio' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
		'gallery',
		'status',
		'audio',
		'chat',
	) );

	/*
	 * This theme styles the visual editor to resemble the theme style,
	 * specifically font, colors, icons, and column width.
	 */
	add_editor_style( array( 'css/editor-style.css', holdfolio_fonts_url() ) );

	// Indicate widget sidebars can use selective refresh in the Customizer.
	add_theme_support( 'customize-selective-refresh-widgets' );
}
endif; // holdfolio_setup
add_action( 'after_setup_theme', 'holdfolio_setup' );

/**
 * Sets the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 *
 * @since Twenty Sixteen 1.0
 */
function holdfolio_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'holdfolio_content_width', 840 );
}
add_action( 'after_setup_theme', 'holdfolio_content_width', 0 );

/**
 * Registers a widget area.
 *
 * @link https://developer.wordpress.org/reference/functions/register_sidebar/
 *
 * @since Twenty Sixteen 1.0
 */
function holdfolio_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'holdfolio' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Add widgets here to appear in your sidebar.', 'holdfolio' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => __( 'Home Page Search', 'holdfolio' ),
		'id'            => 'home-search',
		'description'   => __( 'Appears after menu in homepage', 'holdfolio' ),
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer Disclaimer', 'holdfolio' ),
		'id'            => 'footer-disclaimer',
		'description'   => __( 'Appears at the bottom of the content on footer.', 'holdfolio' ),
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
	) );
}
add_action( 'widgets_init', 'holdfolio_widgets_init' );

if ( ! function_exists( 'holdfolio_fonts_url' ) ) :
/**
 * Register Google fonts for Twenty Sixteen.
 *
 * Create your own holdfolio_fonts_url() function to override in a child theme.
 *
 * @since Twenty Sixteen 1.0
 *
 * @return string Google fonts URL for the theme.
 */
function holdfolio_fonts_url() {
	$fonts_url = '';
	$fonts     = array();
	$subsets   = 'latin,latin-ext';

	/* translators: If there are characters in your language that are not supported by Merriweather, translate this to 'off'. Do not translate into your own language. */
	if ( 'off' !== _x( 'on', 'Merriweather font: on or off', 'holdfolio' ) ) {
		$fonts[] = 'Merriweather:400,700,900,400italic,700italic,900italic';
	}

	/* translators: If there are characters in your language that are not supported by Montserrat, translate this to 'off'. Do not translate into your own language. */
	if ( 'off' !== _x( 'on', 'Montserrat font: on or off', 'holdfolio' ) ) {
		$fonts[] = 'Montserrat:400,700';
	}

	/* translators: If there are characters in your language that are not supported by Inconsolata, translate this to 'off'. Do not translate into your own language. */
	if ( 'off' !== _x( 'on', 'Inconsolata font: on or off', 'holdfolio' ) ) {
		$fonts[] = 'Inconsolata:400';
	}

	if ( $fonts ) {
		$fonts_url = add_query_arg( array(
			'family' => urlencode( implode( '|', $fonts ) ),
			'subset' => urlencode( $subsets ),
		), 'https://fonts.googleapis.com/css' );
	}

	return $fonts_url;
}
endif;

/**
 * Handles JavaScript detection.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 * @since Twenty Sixteen 1.0
 */
function holdfolio_javascript_detection() {
	echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}
add_action( 'wp_head', 'holdfolio_javascript_detection', 0 );

/**
 * Enqueues scripts and styles.
 *
 * @since Twenty Sixteen 1.0
 */
function holdfolio_scripts() {
	// Add custom fonts, used in the main stylesheet.
	wp_enqueue_style( 'holdfolio-fonts', holdfolio_fonts_url(), array(), null );

	// Add Genericons, used in the main stylesheet.
	wp_enqueue_style( 'genericons', get_template_directory_uri() . '/genericons/genericons.css', array(), '3.4.1' );

	// Theme stylesheet.
	wp_enqueue_style( 'holdfolio-style', get_stylesheet_uri() );

	// Load the Internet Explorer specific stylesheet.
	wp_enqueue_style( 'holdfolio-ie', get_template_directory_uri() . '/css/ie.css', array( 'holdfolio-style' ), '20160412' );
	wp_style_add_data( 'holdfolio-ie', 'conditional', 'lt IE 10' );

	// Load the Internet Explorer 8 specific stylesheet.
	wp_enqueue_style( 'holdfolio-ie8', get_template_directory_uri() . '/css/ie8.css', array( 'holdfolio-style' ), '20160412' );
	wp_style_add_data( 'holdfolio-ie8', 'conditional', 'lt IE 9' );

	// Load the Internet Explorer 7 specific stylesheet.
	wp_enqueue_style( 'holdfolio-ie7', get_template_directory_uri() . '/css/ie7.css', array( 'holdfolio-style' ), '20160412' );
	wp_style_add_data( 'holdfolio-ie7', 'conditional', 'lt IE 8' );

	// Load the html5 shiv.
	wp_enqueue_script( 'holdfolio-html5', get_template_directory_uri() . '/js/html5.js', array(), '3.7.3' );
	wp_script_add_data( 'holdfolio-html5', 'conditional', 'lt IE 9' );

	wp_enqueue_script( 'holdfolio-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20160412', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	if ( is_singular() && wp_attachment_is_image() ) {
		wp_enqueue_script( 'holdfolio-keyboard-image-navigation', get_template_directory_uri() . '/js/keyboard-image-navigation.js', array( 'jquery' ), '20160412' );
	}

	wp_enqueue_script( 'holdfolio-script', get_template_directory_uri() . '/js/functions.js', array( 'jquery' ), '20160412', true );

	wp_localize_script( 'holdfolio-script', 'screenReaderText', array(
		'expand'   => __( 'expand child menu', 'holdfolio' ),
		'collapse' => __( 'collapse child menu', 'holdfolio' ),
	) );
}
add_action( 'wp_enqueue_scripts', 'holdfolio_scripts' );

/**
 * Adds custom classes to the array of body classes.
 *
 * @since Twenty Sixteen 1.0
 *
 * @param array $classes Classes for the body element.
 * @return array (Maybe) filtered body classes.
 */
function holdfolio_body_classes( $classes ) {
	// Adds a class of custom-background-image to sites with a custom background image.
	if ( get_background_image() ) {
		$classes[] = 'custom-background-image';
	}

	// Adds a class of group-blog to sites with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	// Adds a class of no-sidebar to sites without active sidebar.
	if ( ! is_active_sidebar( 'sidebar-1' ) ) {
		$classes[] = 'no-sidebar';
	}

	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	return $classes;
}
add_filter( 'body_class', 'holdfolio_body_classes' );

/**
 * Converts a HEX value to RGB.
 *
 * @since Twenty Sixteen 1.0
 *
 * @param string $color The original color, in 3- or 6-digit hexadecimal form.
 * @return array Array containing RGB (red, green, and blue) values for the given
 *               HEX code, empty array otherwise.
 */
function holdfolio_hex2rgb( $color ) {
	$color = trim( $color, '#' );

	if ( strlen( $color ) === 3 ) {
		$r = hexdec( substr( $color, 0, 1 ).substr( $color, 0, 1 ) );
		$g = hexdec( substr( $color, 1, 1 ).substr( $color, 1, 1 ) );
		$b = hexdec( substr( $color, 2, 1 ).substr( $color, 2, 1 ) );
	} else if ( strlen( $color ) === 6 ) {
		$r = hexdec( substr( $color, 0, 2 ) );
		$g = hexdec( substr( $color, 2, 2 ) );
		$b = hexdec( substr( $color, 4, 2 ) );
	} else {
		return array();
	}

	return array( 'red' => $r, 'green' => $g, 'blue' => $b );
}

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for content images
 *
 * @since Twenty Sixteen 1.0
 *
 * @param string $sizes A source size value for use in a 'sizes' attribute.
 * @param array  $size  Image size. Accepts an array of width and height
 *                      values in pixels (in that order).
 * @return string A source size value for use in a content image 'sizes' attribute.
 */
function holdfolio_content_image_sizes_attr( $sizes, $size ) {
	$width = $size[0];

	840 <= $width && $sizes = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 1362px) 62vw, 840px';

	if ( 'page' === get_post_type() ) {
		840 > $width && $sizes = '(max-width: ' . $width . 'px) 85vw, ' . $width . 'px';
	} else {
		840 > $width && 600 <= $width && $sizes = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 984px) 61vw, (max-width: 1362px) 45vw, 600px';
		600 > $width && $sizes = '(max-width: ' . $width . 'px) 85vw, ' . $width . 'px';
	}

	return $sizes;
}
add_filter( 'wp_calculate_image_sizes', 'holdfolio_content_image_sizes_attr', 10 , 2 );

/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for post thumbnails
 *
 * @since Twenty Sixteen 1.0
 *
 * @param array $attr Attributes for the image markup.
 * @param int   $attachment Image attachment ID.
 * @param array $size Registered image size or flat array of height and width dimensions.
 * @return string A source size value for use in a post thumbnail 'sizes' attribute.
 */
function holdfolio_post_thumbnail_sizes_attr( $attr, $attachment, $size ) {
	if ( 'post-thumbnail' === $size ) {
		is_active_sidebar( 'sidebar-1' ) && $attr['sizes'] = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 984px) 60vw, (max-width: 1362px) 62vw, 840px';
		! is_active_sidebar( 'sidebar-1' ) && $attr['sizes'] = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 1362px) 88vw, 1200px';
	}
	return $attr;
}
add_filter( 'wp_get_attachment_image_attributes', 'holdfolio_post_thumbnail_sizes_attr', 10 , 3 );

/**
 * Modifies tag cloud widget arguments to have all tags in the widget same font size.
 *
 * @since Twenty Sixteen 1.1
 *
 * @param array $args Arguments for tag cloud widget.
 * @return array A new modified arguments.
 */
function holdfolio_widget_tag_cloud_args( $args ) {
	$args['largest'] = 1;
	$args['smallest'] = 1;
	$args['unit'] = 'em';
	return $args;
}
add_filter( 'widget_tag_cloud_args', 'holdfolio_widget_tag_cloud_args' );

function new_excerpt_more( $more ) {
    return '';
}
add_filter('excerpt_more', 'new_excerpt_more');


add_action('after_setup_theme', 'remove_admin_bar');

function remove_admin_bar() {
if (!current_user_can('administrator') && !is_admin()) {
  show_admin_bar(false);
}
}

function meks_disable_srcset( $sources ) {
    return false;
}
 
add_filter( 'wp_calculate_image_srcset', 'meks_disable_srcset' );


function get_file_by_name($array,$name)
{
	
	foreach( $array as $key => $each_download ) 
	{
			if($each_download["name"]==$name)
			{
		 	 $file_downlaod='<a href="javascript:void(0);" class="popup_file" data-file="'.$each_download["file"].'">View </a>';
			 break;
			}
			
	}
	
	return $file_downlaod;
}
function download_files($array,$name)
{
	if(count($array)>0)
	{
	$file_downlaod='<h3>Documents</h3>';	
	$file_downlaod.='<ul class="list_n download_docs">';	
	foreach( $array as $key => $each_download ) 
	{
		$pdf_name =$each_download["name"]; 
		$pdf_name = str_replace('-',' ',$pdf_name);
		$pdf_name = str_replace('_',' ',$pdf_name);
		$pdf_name = str_replace('.pdf',' ',$pdf_name);
			if($each_download["name"]!=$name)
			{
		 	 $file_downlaod.=' <li><a href="javascript:void(0);" data-file="'.$each_download["file"].'" class="popup_file"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> '.$pdf_name.' <span class="view_doc_a">View</span></a></li>';
			}
			
	}
	$file_downlaod.='</ul>';
	}
	
	return $file_downlaod;
}


function download_invested_files($array)
{
	if(count($array)>0)
	{
	$file_downlaod='';	
	foreach( $array as $key => $each_download ) 
	{
		
		 	 $file_downlaod.=' <li><a href="javascript:void(0);" data-file="'.$each_download["file"].'" class="popup_file"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> '.$each_download["name"].'  <span class="view_doc_a">View</span></a></li>';
			
			
	}
	}
	
	return $file_downlaod;
}


function get_product_first_img( $post_id ) {
    $args = array(
        'posts_per_page' => 1,
        'order'          => 'ASC',
        'post_mime_type' => 'image',
        'post_parent'    => $post_id,
        'post_status'    => null,
        'post_type'      => 'attachment',
    );
    $attachments = get_children( $args );
    $url= wp_get_attachment_thumb_url( $attachments[0]->ID );
	
	return $url;
    
}

function dizzi_numeric_posts_nav() {

	if( is_singular() )
		return;

	global $wp_query;

	/** Stop execution if there's only 1 page */
	if( $wp_query->max_num_pages <= 1 )
		return;

	$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
	$max   = intval( $wp_query->max_num_pages );

	/**	Add current page to the array */
	if ( $paged >= 1 )
		$links[] = $paged;

	/**	Add the pages around the current page to the array */
	if ( $paged >= 3 ) {
		$links[] = $paged - 1;
		$links[] = $paged - 2;
	}

	if ( ( $paged + 2 ) <= $max ) {
		$links[] = $paged + 2;
		$links[] = $paged + 1;
	}

	echo '<ul class="pagination">' . "\n";

	/**	Previous Post Link */
	if ( get_previous_posts_link() )
		printf( '<li class="npost">%s</li>' . "\n", get_previous_posts_link('<i class="fa fa-angle-double-left"></i> Previous') );

	/**	Link to first page, plus ellipses if necessary */
	if ( ! in_array( 1, $links ) ) {
		$class = 1 == $paged ? ' class="active"' : '';

		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

		if ( ! in_array( 2, $links ) )
			echo '<li>…</li>';
	}

	/**	Link to current page, plus 2 pages in either direction if necessary */
	sort( $links );
	foreach ( (array) $links as $link ) {
		$class = $paged == $link ? ' class="active"' : '';
		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
	}

	/**	Link to last page, plus ellipses if necessary */
	if ( ! in_array( $max, $links ) ) {
		if ( ! in_array( $max - 1, $links ) )
			echo '<li>…</li>' . "\n";

		$class = $paged == $max ? ' class="active"' : '';
		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
	}

	/**	Next Post Link */
	if ( get_next_posts_link() )
		printf( '<li class="npost">%s</li>' . "\n", get_next_posts_link('Next <i class="fa fa-angle-double-right"></i>') );

	echo '</ul>' . "\n";

}

// Custom Functions
function register_holdfolio_session()
{
  if( !session_id() )
  {
    session_start();
  }
}
add_action('init', 'register_holdfolio_session');

wp_enqueue_script('jquery');
add_action('wp_ajax_nopriv_loadInvestment', 'load_investment'); 
add_action('wp_ajax_loadInvestment', 'load_investment');
function load_investment(){
global $wpdb;
$id_product=$_POST['pid'];

$term_list = wp_get_post_terms($id_product,'product_cat',array('fields'=>'ids'));
$cat_id = (int)$term_list[0];
$cat=get_term($cat_id, 'product_cat');
$category=get_term_link ($cat_id, 'product_cat');
$cat_slug=$cat->slug;
if(is_user_logged_in())
{

if($cat_slug=='current-opportunities')
{
	$_SESSION['invest']=array("user"=>get_current_user_id(),"pro_id"=>$id_product,"step1"=>"incomplete","step2"=>"incomplete","step3"=>"incomplete");
	
	
	$res=array("result"=>"ok","redirect"=>get_site_url().'/investment-steps/');
}
else
{
	$res=array("result"=>"denied","message"=>"Invalid Invest response");
}
}
else
{
	$res=array("result"=>"denied","message"=>"Invalid Invest response");
}
$res_array=json_encode($res);
echo $res_array;

die();
}

add_action('wp_ajax_nopriv_loadNext', 'load_next'); 
add_action('wp_ajax_loadNext', 'load_next');
function load_next()
{
	global $wpdb;
	$s1=$_POST['s1'];
	$s2=$_POST['s2'];
	if($s1=='step1')
	{
		$_SESSION['invest']['step1']='completed';
	}
	

	
}

function get_product_attachments_step($post_id)
{
	  $attachments = get_post_meta( $pro_id, '_downloadable_files', true );
     if ( $attachments ) {

        foreach ( $attachments as $attachment ) {

		   
		   echo $attachment->post_title;
          }
     }

}


function download_files_step($array,$name)
{
	if(count($array)>0)
	{
	$file_downlaod='';	
	$file_downlaod.='<ul class="list_n download_docs">';
	$i=0;	
	foreach( $array as $key => $each_download ) 
	{
		
			if($each_download["name"]!=$name)
			{
			
			if(isset($_SESSION['invest']["download"]))
			{
				if(in_array($each_download["file"],$_SESSION['invest']["download"]))
				{
					$data_download='data-downloaded="Yes"';
					$dclass='dowloaded';
				}
				else
				{
					$data_download='data-downloaded="No"';
					$dclass='';
				}
			}
			else
			{
					$data_download='data-downloaded="No"';
					$dclass='';
			}
			
			$fil_name = ($each_download["name"]) ? $each_download["name"] : basename($each_download["file"]);
			
		$fil_name = str_replace('-',' ',$fil_name);
		$fil_name = str_replace('_',' ',$fil_name);
		$fil_name = str_replace('.pdf',' ',$fil_name);
		
		
		$file_downlaod.='<li class="file_'.$i.'"> <i class="fa fa-file-pdf-o" aria-hidden="true"></i> '.$fil_name.'<div class="pull-right"> <a href="javascript:void(0);" 
		 '.$data_download.' class="'.$dclass.'" data-site-url="'.get_site_url().'" data-download-file="'.$each_download["file"].'" data-dwonload-name="'.$each_download["name"].'" data-download-count="'.$i.'"  ><span>View</span></a> </div>
              </li>
			 ';
			}
		$i++;	
	}
	$file_downlaod.='</ul>';
	}
	
	return $file_downlaod;
}

add_action('wp_ajax_nopriv_downloadFile', 'download_step1'); 
add_action('wp_ajax_downloadFile', 'download_step1');


function download_step1()
{
		
		
		$sr=$_POST['sr'];
		$file=$_POST['file'];
		$file_name=$_POST['file_name'];
		$_SESSION['invest']["download"][$sr]=$file;
		$_SESSION['invest']["download_name"][$sr]=$file_name;
		$url_file=get_site_url().'/download_file.php?path='.$file;
		echo $url_file;
		die();
}


function get_attr_byid($arr,$name)
{
	foreach($arr as $a)
	{
		if($a['slug']==$name)
		{
			return $a['value']; 
			break;
		}
	}
}

add_action('wp_ajax_nopriv_loadNextset2', 'load_next_step2'); 
add_action('wp_ajax_loadNextset2', 'load_next_step2');

function load_next_step2()
{
	global $wpdb;
	$inv_as=$_POST['inv_as'];
	$name_inv_as=$_POST['name_inv_as'];
	$inv_amt=$_POST['inv_amt'];
	$ccr_value=$_POST['ccr_value'];
	$total_estimated_value=$_POST['total_estimated_value'];
	if(is_numeric($inv_amt) && $inv_amt>0)
	{
		$pro_id=$_SESSION['invest']['pro_id'];
		$product = get_product( $pro_id );
		$min_investement=$product->get_attribute( 'minimum-investment' );
  		$ccr=$product->get_attribute( 'ccr' );
		$esti_cash=($inv_amt*$ccr)/4;
		
		$_SESSION['invest']['inv_amt']=$inv_amt;
		$_SESSION['invest']['name_inv_as']=$name_inv_as;
		$_SESSION['invest']['inv_as']=$inv_as;
		$_SESSION['invest']['esti_qt_cash']=$esti_cash;
		$_SESSION['invest']['step2']="completed";
		$_SESSION['invest']['ccr_value']=$ccr_value;
		$_SESSION['invest']['total_estimated_value']=$total_estimated_value;
		
		echo "ok";
		die();
	}
	else
	{
		echo "err";
		die();
	}
	
	
}

add_action('wp_ajax_nopriv_loadNextset3', 'load_next_step3'); 
add_action('wp_ajax_loadNextset3', 'load_next_step3');

function load_next_step3()
{
	
	global $wpdb,$current_user;
	get_currentuserinfo();

	$step_email=$_POST['step_email'];
	
	if($step_email!="")
	{
		$_SESSION['invest']['step_email']=$step_email;
		$_SESSION['invest']['step3']="completed";
		
		$pro_id=$_SESSION['invest']['pro_id'];
		$product = get_product( $pro_id );
		$min_investement=$product->get_attribute( 'minimum-investment' );
  		//$ccr=$product->get_attribute( 'ccr' );
		//$esti_cash=($_SESSION['invest']['inv_amt']*$ccr)/4;
		$esti_cash = $_SESSION['invest']['total_estimated_value'];
		
		 $download_files=$_SESSION['invest']['download'];
		 $download_name=$_SESSION['invest']['download_name'];
		
		/*
		$wpdb->insert( 'wp_invested_property', array(
        'user_id' => get_current_user_id(), 
        'pro_id' => $_SESSION['invest']['pro_id'],
        'inv_amount' =>$_SESSION['invest']['inv_amt'], 
        'inv_as' => $_SESSION['invest']['inv_as'],
        'name_inv_as' => $_SESSION['invest']['name_inv_as'], 
        'minimum_inv' => $min_investement,
        'email' => $step_email, 
        'esst_qt_cash' => $esti_cash,
        'download_files' => serialize( $download_files ),
        'download_name' => serialize( $download_name ),
        'added_on' => time(),
        'ip' =>  $_SERVER["REMOTE_ADDR"] 
        ),
        array( '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s') 
    );
    */
    create_invest_order();
		
		//Email To Admin
		
	
	
		
		unset($_SESSION['invest']);
		
		echo "ok";
		die();
	}
	else
	{
		echo "err";
		die();
	}
	
	
}




function get_user_pojects($user_id='',$status = array('wc-pending,','wc-completed'))
{
	$user_id = ($user_id) ? $user_id : get_current_user_id();
$args = array('post_type'=>'shop_order','post_status'=>$status,'meta_query'=>array(
		array(
			'key'     => '_customer_user',
			'value'   => $user_id,
			'compare' => '=',
		),
	));
 $get_invesetments = new WP_Query($args);
 return $get_invesetments->posts;
 
}


add_filter( 'wp_image_editors', 'change_graphic_lib' );

function change_graphic_lib($array) {
	return array( 'WP_Image_Editor_GD', 'WP_Image_Editor_Imagick' );
}



function sub_property_display($product_id)
{
	
	$related_properties = get_post_meta($product_id,'properties',true);
	$rel_pro_html="";
	if(!empty($related_properties)){
		
			$rel_pro_html.='<h2 class="title_group"> PROPERTIES WITHIN THIS PORTFOLIO </h2>';
	 $rel_pro_html.='<div class="row">';
     $rel_pro_html.='<div class="col-md-12 col-sm-12">';
	$rel_pro_html.='<div id="similar_properties_carousel" class="owl-carousel"> ';
		
		foreach($related_properties as $related_propertie){
			$pdf_url = (is_numeric ($related_propertie['pdf'])) ? ot_meta_attachment_by_id($related_propertie['pdf']) : $related_propertie['pdf'];
			$image_url = (is_numeric ($related_propertie['image'])) ? ot_meta_attachment_by_id($related_propertie['image']) : $related_propertie['image'];
			$pdf_url = do_shortcode($pdf_url);
			$image_url = do_shortcode($image_url);
			
			$rel_pro_html.='  
				<div class="item"> 
                  <a data-toggle="modal"  class="load_ppopup" href="#myModal" data-file="'.$image_url.'"  data-title="'.$related_propertie['title'].'"><img src="'.$image_url.'" class="img-responsive wp-post-image" alt="11" /></a>
                  <h4>'.$related_propertie['title'].' </h4>
                  <p><a data-toggle="modal"  class="load_ppopup"  href="#myModal" data-file="'.$pdf_url.'"  data-title="'.$pdf_url.'">Read More</a></p>
				  </div>';
			
		}
		
		$rel_pro_html.=' <div class="customNavigation text-center"> <a class="prev"></a> <a class="next"></a> </div>';
	$rel_pro_html.='</div>';
	$rel_pro_html.='</div>';	
	$rel_pro_html.='</div>';
	}
	/*
	$product = get_product( $product_id );	
    $upsells = $product->get_upsells();
   if(!$upsells)
   {
       $upsell_pro_html="";
   }
   else
   {

	$meta_query = WC()->query->get_meta_query();

    $args = array(
        'post_type' => 'product',
        'ignore_sticky_posts' => 1,
        'no_found_rows' => 1,
        'posts_per_page' => $posts_per_page,
        'orderby' => $orderby,
        'post__in' => $upsells,
        'post__not_in' => array($product_id),
        'meta_query' => $meta_query
    );

    $products = new WP_Query($args);
	$upsell_pro_html='';
    if ($products->have_posts()) :
      $upsell_pro_html.='<h2 class="title_group"> PROPERTIES WITHIN THIS PORTFOLIO </h2>';
	 $upsell_pro_html.='<div class="row">';
     $upsell_pro_html.='<div class="col-md-12 col-sm-12">';
	
	$upsell_pro_html.='<div id="similar_properties_carousel" class="owl-carousel"> ';
	
			while ( $products->have_posts() ) : $products->the_post();
			
			$pro_id=get_the_ID();
			$pro = get_product( $pro_id );			
			$src_past = $pro->get_image('default',array("class"=>"img-responsive"));
			$pro_title=get_the_title($pro_id);

			$array_files = get_post_meta( $pro_id, '_downloadable_files', true );
			foreach( $array_files as $key => $each_download ) 
			{
				$attachment_title=$each_download['name'];
				$attachment_file=$each_download['file'];
			}
           

			$upsell_pro_html.='  
				<div class="item"> 
                  <a data-toggle="modal"  class="load_ppopup" href="#myModal" data-file="'.$attachment_file.'"  data-title="'.$attachment_title.'">'.$src_past.'</a>
                  <h4>'.$pro_title.' </h4>
                  <p><a data-toggle="modal"  class="load_ppopup"  href="#myModal" data-file="'.$attachment_file.'"  data-title="'.$attachment_title.'">Read More</a></p>
				  </div>';
			 endwhile; // end of the loop. 
	
	$upsell_pro_html.=' <div class="customNavigation text-center"> <a class="prev"></a> <a class="next"></a> </div>';
	$upsell_pro_html.='</div>';
	$upsell_pro_html.='</div>';	
	$upsell_pro_html.='</div>';

   endif;
   }
   */
   return $rel_pro_html;
}




function user_img_url($current_user_id)
{
	$img=get_the_author_meta('image',$current_user_id);
	if($img=='')
	{
		$img_url=get_template_directory_uri().'/images/icon-user-default.png';
	}
	else
	{
		$img_url=$img;
	}
	return $img_url;
	
	
}

add_action('wp_ajax_nopriv_loadRevSpot', 'rev_spot'); 
add_action('wp_ajax_loadRevSpot', 'rev_spot');


function rev_spot()
{
	global $wpdb,$current_user;
	get_currentuserinfo();
	
	
	
	$inv_as=$_POST['inv_as'];
	$name_inv_as=$_POST['name_inv_as'];
	$inv_amt=$_POST['inv_amt'];
	$pro_id=$_POST['pro_id'];
	$product = get_product( $pro_id );
	$min_investement=$product->get_attribute('minimum-investment' );
  	$ccr=$product->get_attribute( 'ccr' );
	$esti_cash=($inv_amt*$ccr)/4;
	
	 $wpdb->insert( 'wp_reserv_spot', array(
        'user_id' => get_current_user_id(), 
        'pro_id' => $pro_id,
        'inv_amt' =>$inv_amt, 
        'inv_as' => $inv_as,
        'name_inv_as' => $name_inv_as, 
        'minimum_inv' => $min_investement,
        'ccr' => $ccr,
        'added_on' => time(),
        'ip' =>  $_SERVER["REMOTE_ADDR"] 
        ),
        array( '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s') 
    );
	
		$project_name=get_the_title($pro_id);
		$user_name=$current_user->user_firstname.' '.$current_user->user_lastname;
		
		$to ='tilakaman@gmail.com';
		
		$subject = "New Spot Reservation Request by {$user_name}";
		
		$mail_body="<p>Dear Admin,</p><br>";
		$mail_body.= "New Spot Reservation Request by {$user_name}";
		
		$mail_body.="<h4>Investment Details</h4>";
		$mail_body.="<p><strong>Project name<strong>: {$project_name}</p>";
		$mail_body.="<p><strong>Investing As <strong>:".$inv_as."</p>";
		if($investing_as=='An LLC, Corporation, or Partnership')
		{
		$mail_body.="<p><strong>Name of LLC/Corp/Partnership' <strong>:".$name_inv_as."</p>";
		}
		elseif($investing_as=='A Trust')
		{
		$mail_body.="<p><strong>Name of Trust'<strong>:".$name_inv_as."</p>";
		}
		$mail_body.="<p><strong>Amount Investing'<strong>: $".$inv_amt."</p>";
		
		$message = $mail_body;
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$headers .= 'From: Holdfolio <info@holdfolio.com>' . "\r\n";
		$flag = mail($to, $subject, $message, $headers,'-finfo@holdfolio.com');
		$user_email=$current_user->user_email;
		
		$user_to=$user_email;
		$user_subject = "Spot Reservation";	
		$mail_body_user="<p>Dear {$user_name},</p><br>";
		$mail_body_user.="<h4>Thank you for your spot reservation on {$project_name}</h4>";
		
		$message = $mail_body_user;
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$headers .= 'From: Holdfolio <info@holdfolio.com>' . "\r\n";
		$flag = mail($user_to, $user_subject, $message, $headers,'-finfo@holdfolio.com');
		echo  "ok";
		exit;
	

}


function check_invested_prop($user_id,$prop_id)
{
	$user_id = ($user_id) ? $user_id : get_current_user_id();
/*	global $wpdb;
$querystr = " SELECT wp_invested_property.* 
    FROM wp_invested_property
    WHERE wp_invested_property.user_id = {$user_id} and wp_invested_property.pro_id = {$prop_id}
    ORDER BY id DESC";

 $results = $wpdb->get_results($querystr, OBJECT);
 print_r($results);
 */
 $args = array('post_type'=>'shop_order','post_status'=>array('wc-pending,','wc-completed'),'meta_query'=>array(
 	'relation'=>'AND',
		array(
			'key'     => '_invest_product_id',
			'value'   => $prop_id,
			'compare' => '=',
		),
		array(
			'key'     => '_customer_user',
			'value'   => $user_id,
			'compare' => '=',
		),
	));
 $get_invesetments = new WP_Query($args);
 return $get_invesetments->post;
}

function check_reserved_prop($user_id,$prop_id)
{
	global $wpdb;
$querystr = " SELECT * 
    FROM wp_reserv_spot
    WHERE wp_reserv_spot.user_id = {$user_id} and wp_reserv_spot.pro_id = {$prop_id}
    ORDER BY id DESC";
 $results = $wpdb->get_results($querystr, OBJECT);
 
 
 return $results;
}

function get_current_pros($user_id)
{
	$html="";
 $args = array(
            'posts_per_page' => -1,
            'tax_query' => array(
                'relation' => 'AND',
                array(
                    'taxonomy' => 'product_cat',
                    'field' => 'slug',
                    'terms' => "current-opportunities"
                )
            ),
            'post_type' => 'product',
            'orderby' => 'title,'
        );	
	$loop = new WP_Query( $args );
	$current_opportunities_products = array();
 	 while ( $loop->have_posts() ) : $loop->the_post(); 
 	 
 	 global $product;
 	 $pro_id=$loop->post->ID;
			$proa = get_product($pro_id);
			
			$check_pro=check_invested_prop($user_id,$pro_id);
			
			if(count($check_pro)==0)
			{
				$current_opportunities_products[] = $proa;
			}
 	 	
		endwhile;
            
			
			if(!empty($current_opportunities_products)){
				$html .= '<div class="bg-white mrgnB15 current_oppor_slider">
        <h3 class="sub_title" 8=""> <img src="'. get_template_directory_uri().'/images/professional-advance.png" alt="">Current Opportunities</h3>
		
		
	<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
	  <!-- Wrapper for slides -->
	  <div class="carousel-inner" role="listbox">';
				foreach($current_opportunities_products as $pro){
					
			
			
			$src = $pro->get_image('default',array("class"=>"img-big-width"));
			$src_past = $pro->get_image('default',array("class"=>"img-responsive"));
			$address = $pro->get_attribute( 'address' );
			
			
			$minimum_investment = $pro->get_attribute( 'minimum-investment' );
			$irr_from = $product->get_attribute( 'irr-from' );
			$irr_to = $product->get_attribute( 'irr-to' );
			$no_properties= $product->get_attribute( 'no-of-properties' );
		    $property_type= $product->get_attribute( 'property-type' ); 
			$property_term = $pro->get_attribute( 'property-term' );
			$estimated_market_value = $pro->get_attribute( 'estimated-fair-market-value' );
			$detail_url = get_permalink( $pro_id );
			$project_name=get_the_title($pro_id);
			
			
			$html.='<div class="item">
					<div class="inv_item_block">
					<div class="image_block">
						'.$src_past.'
						<div class="img_content_blk">
							<label>'.$address.'</label>
							<span>'.$project_name.'</span>
						</div>
					</div>
					<!-- // image_block -->
					
					<div class="invest_brif">
						<div class="row">
							<div class="col-md-3 col-sm-3 col-xs-12 brder_r">
								<label>IRR</label>
								<span class="label_value">'.$irr_from.'% - '.$irr_to.'%</span>
							</div>
							<div class="col-md-4 col-sm-4 col-xs-12 brder_r">
								<label>Min. Investment</label>
								<span class="label_value">$'.$minimum_investment.'</span>
							</div>
							<div class="col-md-5 col-sm-5 col-xs-12 brder_r">
								<label>Cash on Cash Return</label>
								<span class="label_value">10% - 12%</span>
							</div>
						</div>
						<!--// end row -->
					</div>
					</div>
					</div>';
		
				}
				
				$html .= '
	  </div>
		<div class="slider-control">
			 <!-- Controls -->
		  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
			<i class="fa fa-angle-left" aria-hidden="true"></i>
		  </a>
		  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
			<i class="fa fa-angle-right" aria-hidden="true"></i>
		  </a>
		</div>
		</div>';
			}
				
				
	
 
 return $html;
}
