<?php

include_once('emails/holdfolio_emails.php');

function create_invest_order(){
	
	global $wpdb,$current_user;
	get_currentuserinfo();
    $address = array(
            'first_name' => '',
            'last_name'  => '',
            'company'    => '',
            'email'      => $_SESSION['invest']['step_email'],
            'phone'      => '',
            'address_1'  => '',
            'address_2'  => '', 
            'city'       => '',
            'state'      => '',
            'postcode'   => '',
            'country'    => ''
        );
        add_filter('woocommerce_get_price','change_price_regular_member', 10, 2);
        function change_price_regular_member($price){
            if(isset($_SESSION['invest']['inv_amt']) && $_SESSION['invest']['inv_amt'] !=''){
               $price =  $_SESSION['invest']['inv_amt'];
            }
            return $price;
        }
        $order = wc_create_order(array('customer_id'=>get_current_user_id(),'parent'=>$_SESSION['invest']['pro_id']));
        $order->add_product( get_product( $_SESSION['invest']['pro_id'] ), 1 ); //(get_product with id and next is for quantity)
        $order->set_address( $address, 'billing' );
        $order->set_address( $address, 'shipping' );
        $order->calculate_totals();
        add_post_meta($order->id,'_invest_details',$_SESSION);
        add_post_meta($order->id,'_invest_product_id',$_SESSION['invest']['pro_id']);
        add_post_meta($order->id,'_customer_user',$current_user->ID);
        
		do_action('holdfolio_after_new_investment_request',$order->id,$_SESSION);
        return $order->id;
}
function hld_get_custom_logo($html){
   
   $html = strip_tags($html,'<img>');
   if(is_user_logged_in() && get_orders_by_user_id()){
   $html = '<a href="'.site_url().'" class="custom-logo">'.$html.'</a>';
       
   }else{
   $html = '<a href="'.site_url().'" class="custom-logo">'.$html.'</a>';
       
   }
return $html;
}
add_filter('get_custom_logo','hld_get_custom_logo');

function get_orders_by_user_id($user_id=''){
$user_id = ($user_id) ? $user_id : get_current_user_id();
    
 $args = array('post_type'=>'shop_order','post_status'=>'any','meta_query'=>array(
		array(
			'key'     => '_customer_user',
			'value'   => $user_id,
			'compare' => '=',
		),
	));
 $get_invesetments = new WP_Query($args);
 return $get_invesetments->post;
}

add_filter( 'template_redirect', 'my_login_page', 50, 3 );
function my_login_page( $defaults ) {
    if(is_page('dashboard')){
         if(get_orders_by_user_id()){
         }else{
        wp_redirect(site_url('/invest'));
           exit;
         }
    }
}
add_filter('body_class','no_inverstment_yet');
function no_inverstment_yet($classes){
    
    if(get_orders_by_user_id()){
        $classes[] ='already_an_investor';
    }else{
        $classes[] = 'not_an_investor';
    }
    return $classes;
}
function get_ccr_value_by_id($id=''){
     $prd_id = (isset($_SESSION['invest']['pro_id']) && $_SESSION['invest']['pro_id'] !='') ? $_SESSION['invest']['pro_id'] : null;
           
           $prd_id = ($id) ? $id : $prd_id;
            if($prd_id){
            	$pro = wc_get_product( $prd_id );
            		$ccr_from = $pro->get_attribute( 'ccr_from' );
            		$ccr_to = $pro->get_attribute( 'ccr_to' );
            		if($ccr_to !=''){
            		  $ccr = ($ccr_from+$ccr_to)/2;
            		}else{
            		  $ccr = $ccr_from;
            		  
            		}
            		
            		return $ccr;
            }
}

function hold_woocommerce_register_post_type_product($labels){
    	$permalinks        = get_option( 'woocommerce_permalinks' );
		$product_permalink = empty( $permalinks['product_base'] ) ? _x( 'product', 'slug', 'woocommerce' ) : $permalinks['product_base'];

	
    
   $labels=  array(
					'labels'              => array(
							'name'                  => __( 'Portfoilos', 'woocommerce' ),
							'singular_name'         => __( 'Portfoilo', 'woocommerce' ),
							'menu_name'             => _x( 'Portfoilos', 'Admin menu name', 'woocommerce' ),
							'add_new'               => __( 'Add Portfoilo', 'woocommerce' ),
							'add_new_item'          => __( 'Add New Portfoilo', 'woocommerce' ),
							'edit'                  => __( 'Edit', 'woocommerce' ),
							'edit_item'             => __( 'Edit Portfoilo', 'woocommerce' ),
							'new_item'              => __( 'New Portfoilo', 'woocommerce' ),
							'view'                  => __( 'View Portfoilo', 'woocommerce' ),
							'view_item'             => __( 'View Portfoilo', 'woocommerce' ),
							'search_items'          => __( 'Search Portfoilos', 'woocommerce' ),
							'not_found'             => __( 'No Portfoilos found', 'woocommerce' ),
							'not_found_in_trash'    => __( 'No Portfoilos found in trash', 'woocommerce' ),
							'parent'                => __( 'Parent Portfoilo', 'woocommerce' ),
							'featured_image'        => __( 'Portfoilo Image', 'woocommerce' ),
							'set_featured_image'    => __( 'Set portfolio image', 'woocommerce' ),
							'remove_featured_image' => __( 'Remove portfolio image', 'woocommerce' ),
							'use_featured_image'    => __( 'Use as portfolio image', 'woocommerce' ),
							'insert_into_item'      => __( 'Insert into portfolio', 'woocommerce' ),
							'uploaded_to_this_item' => __( 'Uploaded to this portfolio', 'woocommerce' ),
							'filter_items_list'     => __( 'Filter portfolios', 'woocommerce' ),
							'items_list_navigation' => __( 'Portfoilos navigation', 'woocommerce' ),
							'items_list'            => __( 'Portfoilos list', 'woocommerce' ),
						),
					'description'         => __( 'This is where you can add new portfolios to your store.', 'woocommerce' ),
					'public'              => true,
					'show_ui'             => true,
					'capability_type'     => 'product',
					'map_meta_cap'        => true,
					'publicly_queryable'  => true,
					'exclude_from_search' => false,
					'hierarchical'        => false, // Hierarchical causes memory issues - WP loads all records!
					'rewrite'             => $portfolio_permalink ? array( 'slug' => untrailingslashit( $portfolio_permalink ), 'with_front' => false, 'feeds' => true ) : false,
					'query_var'           => true,
					'supports'            => array( 'title', 'editor', 'excerpt', 'thumbnail', 'comments', 'custom-fields', 'page-attributes', 'publicize', 'wpcom-markdown' ),
					'has_archive'         => ( $shop_page_id = wc_get_page_id( 'shop' ) ) && get_post( $shop_page_id ) ? get_page_uri( $shop_page_id ) : 'shop',
					'show_in_nav_menus'   => true
				);
				return $labels;
				
}
add_filter('woocommerce_register_post_type_product','hold_woocommerce_register_post_type_product');

function hold_woocommerce_taxonomy_args_product_cat($labels){
		$permalinks = get_option( 'woocommerce_permalinks' );
   $labels =  array(
				'hierarchical'          => true,
				'update_count_callback' => '_wc_term_recount',
				'label'                 => __( 'Opportunities', 'woocommerce' ),
				'labels' => array(
						'name'              => __( 'Opportunities', 'woocommerce' ),
						'singular_name'     => __( 'Opportunity', 'woocommerce' ),
						'menu_name'         => _x( 'Opportunities', 'Admin menu name', 'woocommerce' ),
						'search_items'      => __( 'Search Opportunities', 'woocommerce' ),
						'all_items'         => __( 'All Opportunities', 'woocommerce' ),
						'parent_item'       => __( 'Parent Opportunity', 'woocommerce' ),
						'parent_item_colon' => __( 'Parent Opportunity:', 'woocommerce' ),
						'edit_item'         => __( 'Edit Opportunity', 'woocommerce' ),
						'update_item'       => __( 'Update Opportunity', 'woocommerce' ),
						'add_new_item'      => __( 'Add New Opportunity', 'woocommerce' ),
						'new_item_name'     => __( 'New Opportunity Name', 'woocommerce' ),
						'not_found'         => __( 'No Opportunity found', 'woocommerce' ),
					),
				'show_ui'               => true,
				'query_var'             => true,
				'capabilities'          => array(
					'manage_terms' => 'manage_product_terms',
					'edit_terms'   => 'edit_product_terms',
					'delete_terms' => 'delete_product_terms',
					'assign_terms' => 'assign_product_terms',
				),
				'rewrite'               => array(
					'slug'         => empty( $permalinks['category_base'] ) ? _x( 'product-category', 'slug', 'woocommerce' ) : $permalinks['category_base'],
					'with_front'   => false,
					'hierarchical' => true,
				),
			);
			return $labels;
}
add_filter('woocommerce_taxonomy_args_product_cat','hold_woocommerce_taxonomy_args_product_cat');

function reserve_a_product(){
	global $wpdb,$current_user;
	get_currentuserinfo();
	if(isset($_REQUEST['reserve_amount']) && $_REQUEST['reserve_amount'] !=''){
        	$_SESSION['invest'] =$_REQUEST;
            $_SESSION['invest']['step_email'] = '';
            $_SESSION['invest']['inv_amt'] = $_REQUEST['reserve_amount'];
            $_SESSION['invest']['pro_id'] = $_REQUEST['pro_id'];
	        $args= array(
	            'customer_id'=>get_current_user_id(),
	            'parent'=>$_SESSION['invest']['pro_id']
	            );
	    
	    if(isset($_REQUEST['order_id']) && $_REQUEST['order_id'] !=''){
	        $args['order_id'] = $_REQUEST['order_id'];
	        update_post_meta($args['order_id'],'_order_total',$_REQUEST['reserve_amount']);
            update_post_meta($args['order_id'],'_total_estimated_value',$_REQUEST['total_estimated_value']);
		do_action('holdfolio_after_update_reserve_request',$order->id,$_SESSION);
	    }else{
            		$address = array(
                        'first_name' => '',
                        'last_name'  => '',
                        'company'    => '',
                        'email'      => $current_user->user_email,
                        'phone'      => '',
                        'address_1'  => '',
                        'address_2'  => '', 
                        'city'       => '',
                        'state'      => '',
                        'postcode'   => '',
                        'country'    => ''
                    );
                    add_filter('woocommerce_get_price','change_price_regular_member', 10, 2);
                    function change_price_regular_member($price){
                        if(isset($_SESSION['invest']['inv_amt']) && $_SESSION['invest']['inv_amt'] !=''){
                           $price =  $_SESSION['invest']['inv_amt'];
                        }
                        return $price;
                    }
                      $order = wc_create_order($args);
                    $order->add_product( get_product( $_SESSION['invest']['pro_id'] ), 1 ); //(get_product with id and next is for quantity)
                    $order->set_address( $address, 'billing' );
                    $order->set_address( $address, 'shipping' );
                    $order->calculate_totals();
                    add_post_meta($order->id,'_invest_details',$_SESSION);
                    add_post_meta($order->id,'_invest_product_id',$_SESSION['invest']['pro_id']);
                    add_post_meta($order->id,'_is_reserved','true');
                    add_post_meta($order->id,'_total_estimated_value',$_REQUEST['total_estimated_value']);
        			add_post_meta($order->id,'_customer_user',$current_user->ID);
        			
        			
		do_action('holdfolio_after_new_reserve_request',$order->id,$_SESSION);
                    
                  
	    }
                    
	    
	}
}
add_filter('template_redirect','reserve_a_product');

function hold_woocommerce_register_post_type_shop_order($array){
    $array['labels']=array(
							'name'                  => __( 'Investment Requests', 'woocommerce' ),
							'singular_name'         => _x( 'Investment Request', 'shop_order post type singular name', 'woocommerce' ),
							'add_new'               => __( 'Add Investment Request', 'woocommerce' ),
							'add_new_item'          => __( 'Add New Investment Request', 'woocommerce' ),
							'edit'                  => __( 'Edit', 'woocommerce' ),
							'edit_item'             => __( 'Edit Investment Request', 'woocommerce' ),
							'new_item'              => __( 'New Investment Request', 'woocommerce' ),
							'view'                  => __( 'View Investment Request', 'woocommerce' ),
							'view_item'             => __( 'View Investment Request', 'woocommerce' ),
							'search_items'          => __( 'Search Investment Requests', 'woocommerce' ),
							'not_found'             => __( 'No Investment Requests found', 'woocommerce' ),
							'not_found_in_trash'    => __( 'No Investment Requests found in trash', 'woocommerce' ),
							'parent'                => __( 'Parent Investment Requests', 'woocommerce' ),
							'menu_name'             => _x( 'Investment Requests', 'Admin menu name', 'woocommerce' ),
							'filter_items_list'     => __( 'Filter invest requests', 'woocommerce' ),
							'items_list_navigation' => __( 'Investment Requests navigation', 'woocommerce' ),
							'items_list'            => __( 'Investment Requests list', 'woocommerce' ),
						);
    return $array;
}
add_filter('woocommerce_register_post_type_shop_order','hold_woocommerce_register_post_type_shop_order');

add_action('admin_menu', 'hold_register_my_custom_submenu_page');
 
function hold_register_my_custom_submenu_page() {
    add_submenu_page(
        'edit.php?post_type=product',
        'Investment Requests',
        'Investment Requests',
        'manage_options',
        'edit.php?post_type=shop_order' );
    add_submenu_page(
        'edit.php?post_type=product',
        'Investment Reports',
        'Investment Reports',
        'manage_options',
        'admin.php?page=wc-reports' );
    add_submenu_page(
        'edit.php?post_type=product',
        'Settings',
        'Settings',
        'manage_options',
        'admin.php?page=wc-settings' );
}


function holdfolio_set_html_mail_content_type() {
    return 'text/html';
}
add_filter( 'wp_mail_from_name', 'holdfolio_wp_mail_from_name' );
function holdfolio_wp_mail_from_name( $original_email_from ) {
	return 'Holdfolio Team';
}

add_filter( 'wp_mail_from', 'holdfolio_wp_mail_from' );
function holdfolio_wp_mail_from( $original_email_address ) {
	//Make sure the email is from the same domain 
	//as your website to avoid being marked as spam.
	return 'noreply@armentum.co';
}

function hold_wc_order_statuses($status){
		$order_statuses = array(
		'wc-pending'    => _x( 'Pending Payment', 'Order status', 'woocommerce' ),
		'wc-completed'  => _x( 'Approved', 'Order status', 'woocommerce' ),
		'wc-cancelled'  => _x( 'Cancelled', 'Order status', 'woocommerce' ),
	);
	return $order_statuses;
}
add_filter('wc_order_statuses','hold_wc_order_statuses');

/**
 * Filters the required title field's label.
 */
function filter_list_item_title_label( $label, $id ) {

  if ( $id == 'earnings' ) {

    $label = __( 'Payment Date', 'theme-domain' );

  }

  return $label;

}
add_filter( 'ot_list_item_title_label', 'filter_list_item_title_label', 10, 2 );

/**
 * Filters the required title field's description.
 */
function filter_list_item_title_desc( $label, $id ) {

  if ( $id == 'earnings' ) {

    $label = __( 'Specify the date', 'theme-domain' );

  }

  return $label;

}
add_filter( 'ot_list_item_title_desc', 'filter_list_item_title_desc', 10, 2 );

function ot_list_item_title_type( $type, $id ) {

  if ( $id == 'earnings' ) {

    $type = 'date-picker';

  }

  return $type;

}
add_filter( 'ot_list_item_title_type', 'ot_list_item_title_type', 10, 2 );

function steps_navigation(){
	if($_GET['step']==1){
		
		$_SESSION['invest']['step1']='incomplete';
		$_SESSION['invest']['step2']="incomplete";
		$_SESSION['invest']['step3']="incomplete";
		//unset($_SESSION['invest']['step1']);
		//unset($_SESSION['invest']['step2']);
		//unset($_SESSION['invest']['step3']);
	}
	
	if($_GET['step']==2){
		
	//	$_SESSION['invest']['step1']='incomplete';
		$_SESSION['invest']['step2']="incomplete";
		$_SESSION['invest']['step3']="incomplete";
		//unset($_SESSION['invest']['step1']);
		//unset($_SESSION['invest']['step2']);
		//unset($_SESSION['invest']['step3']);
	}
}
add_filter('template_redirect','steps_navigation');

function hold_exclude_posts($query) {
	//exit;
    global $wpdb;
			$t_posts = $wpdb->posts;
			$t_meta = $wpdb->prefix . "postmeta";
			
				$user_id = get_current_user_id();
				
	
		$all_private_post_ids = array();
    if ( !is_admin() ) {			
	if ( !is_user_logged_in() ) {
     $sql="SELECT ID FROM $wpdb->posts as wp INNER JOIN $t_meta as mt on mt.post_id=wp.ID where mt.meta_key='choose_a_visibility_type' and mt.meta_value='registered_users'";
		$all_private_posts = $wpdb->get_results($sql, ARRAY_A   );
		if(!empty($all_private_posts)){
    		foreach( $all_private_posts as $result )
    		$all_private_post_ids[] = $result[ID];
		}
		
		
  }
  
  
	
        
        
				$is_accredited = get_user_meta($user_id,'accdt_inv',true);
				if($is_accredited=='Yes'){
					
     $sql="SELECT ID FROM $wpdb->posts as wp INNER JOIN $t_meta as mt on mt.post_id=wp.ID where mt.meta_key='choose_a_visibility_type' and mt.meta_value='non_accredited'";
		$all_private_posts = $wpdb->get_results($sql, ARRAY_A   );
		
		if(!empty($all_private_posts)){
    		foreach( $all_private_posts as $result )
    		$all_private_post_ids[] = $result[ID];
		}
				}else{
					 $sql="SELECT ID FROM $wpdb->posts as wp INNER JOIN $t_meta as mt on mt.post_id=wp.ID where mt.meta_key='choose_a_visibility_type' and mt.meta_value='accredited'";
		$all_private_posts = $wpdb->get_results($sql, ARRAY_A   );
		
		if(!empty($all_private_posts)){
    		foreach( $all_private_posts as $result )
    		$all_private_post_ids[] = $result[ID];
		}
				}
				
        $query->set('post__not_in', $all_private_post_ids);		
	}
  
  
}
add_action('pre_get_posts', 'hold_exclude_posts');



function tss_login_page() 
{
	$url = basename($_SERVER['REQUEST_URI']);
	$slug = strtok($url ,'?' );
	$action = $_REQUEST['action'];
	$login_page = site_url( '/login/');
	if( $slug == "wp-login.php" && $action == "" && $_SERVER['REQUEST_METHOD'] == 'GET' ) 
	{
		wp_redirect($login_page);
		exit;
	}
}
add_action('init','tss_login_page');

function tss_login_failed()
{
	$query="";
$query_varsa = parse_url($_REQUEST['redirect_to']);
$query = $query_varsa['query'];
    if(isset($query) && $query !=''){
        $pack = $_REQUEST['pack'];
        $query = str_replace('&err=true','',$query);
        $query .='&err=true';
    }else{
        $query ='?err=true';
    }
	$login_failed_url = site_url( '/login/').' ?'.$query;
	wp_redirect( $login_failed_url );
	exit;
}
add_action( 'wp_login_failed', 'tss_login_failed' );

function tss_blank_username_password( $user, $username, $password ) 
{




	if( $username == "" || $password == "" ) 
	{
	
	$query="";
$query_varsa = parse_url($_REQUEST['redirect_to']);
$query = $query_varsa['query'];
    if(isset($query) && $query !=''){
        $pack = $_REQUEST['pack'];
        $query = str_replace('&err=true','',$query);
        $query .='&err=true';
    }else{
        $query ='?err=true';
    }
	$login_failed_url = site_url( '/login/').' ?'.$query;
		wp_redirect( $login_failed_url );
		exit;
	}
}
add_filter( 'authenticate', 'tss_blank_username_password', 1, 3);

function tss_login_with_email_address( &$username ) {
    $user = get_user_by( 'email', $username );
    if ( !empty( $user->user_login ) )
        $username = $user->user_login;
    return $username;
}
add_action( 'wp_authenticate','tss_login_with_email_address' );


function hold_price($price,$echo=1,$show_currency=true){
	if($show_currency==true){
		$currency = '$';
	}else{
		$currency ="";
	}
	//$price = ($price !='') ? $currency.bd_nice_number($price) : '-';
	$price = wc_price($price);
	
	$price = str_replace('<span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&#36;</span>','',$price);
	$price = str_replace('</span></span>','',$price);
	$price = '&#36;'.$price;
	if($echo==1){
		echo $price;
	}else{
		return $price;
	}
}
function hold_percentage($price,$echo=1){
	$price = number_format($price,2);
	$price = ($price !='') ? $price.'%' : '';
	if($echo==1){
		echo $price;
	}else{
		return $price;
	}
}
function bd_nice_number($n) {
        // first strip any formatting;
        $n = (0+str_replace(",","",$n));
        
        // is this a number?
        if(!is_numeric($n)) return false;
        
        // now filter it;
        if($n>1000000000000) return round(($n/1000000000000),1).'tr';
        else if($n>1000000000) return round(($n/1000000000),1).'bi';
        else if($n>1000000) return round(($n/1000000),1).'mi';
        else if($n>1000) return round(($n/1000),1).'k';
        
        return number_format($n);
    }

function my_page_template_redirect()
{
	$url = $_SERVER['REQUEST_URI'];
	
    if($url=='/my-account/' || $url=='/holdfolio/my-account/')
    {
        wp_redirect( home_url( '/login/' ) );
        exit();
    }
}
add_action( 'template_redirect', 'my_page_template_redirect' );

add_filter( 'ot_type_date_picker_date_format', 'hold_modify_date_picker_date_format', 10, 2 );
function hold_modify_date_picker_date_format( $format, $field_id ) {
    return 'mm/dd/yy';
}

function get_all_orders_of_a_portfolio($project_id){
	global $wpdb;
			$t_posts = $wpdb->posts;
			$t_order_items = $wpdb->prefix . "woocommerce_order_items";
			$t_term_rel = $wpdb->prefix . "term_relationships";    
			$t_order_itemmeta = $wpdb->prefix . "woocommerce_order_itemmeta";
              	$sql="SELECT $t_order_items.order_id FROM $t_order_items 
		LEFT JOIN $t_order_itemmeta on $t_order_itemmeta.order_item_id=$t_order_items.order_item_id
		WHERE $t_order_items.order_item_type='line_item' AND $t_order_itemmeta.meta_key='_product_id' AND $t_order_itemmeta.meta_value IN ($project_id) order by  $t_order_items.order_id desc";
		
		$all_posts = $wpdb->get_results($sql, ARRAY_A);
		
			$all_email_ids = array();
		foreach($all_posts as $ap){
		    $order_id = $ap['order_id'];
		    $new_order = new WC_Order($order_id);
		   if($new_order->post_status=='wc-completed'){
		   	$all_email_ids[]= $order_id;
		   }
		   }
		return $all_email_ids;
}

add_filter( 'gettext', 'my_wp_text_email_filtering', 10, 3 );

function my_wp_text_email_filtering( $translated, $text, $domain ) {
	if($translated =="Order date:"){
		$translated ="Created Date";
	}
	if($translated =="Order status:"){
		$translated ="Invest status:";
	}
	if($translated =="Customer:"){
		$translated ="Invester:";
	}
    return $translated;
}
function hold_ot_upload_text($choose_image){
	$choose_image ="Choose Selected";
	return $choose_image;
}
add_filter('ot_upload_text','hold_ot_upload_text');

function hld_setup() {
    add_theme_support('custom-logo');
}

add_action('after_setup_theme', 'hld_setup');


add_image_size('mytheme-logo', 300, 93);
add_theme_support('custom-logo', array(
    'size' => 'mytheme-logo'
));

function hold_wp_login_form( $args = array() ) {
    $defaults = array(
        'echo' => true,
        // Default 'redirect' value takes the user back to the request URI.
        'redirect' => ( is_ssl() ? 'https://' : 'http://' ) . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'],
        'form_id' => 'loginform',
        'label_username' => __( 'Username or Email' ),
        'label_password' => __( 'Password' ),
        'label_remember' => __( 'Remember Me' ),
        'label_log_in' => __( 'Log In' ),
        'id_username' => 'user_login',
        'id_password' => 'user_pass',
        'id_remember' => 'rememberme',
        'id_submit' => 'wp-submit',
        'remember' => true,
        'value_username' => '',
        // Set 'value_remember' to true to default the "Remember me" checkbox to checked.
        'value_remember' => false,
    );
 
    /**
     * Filters the default login form output arguments.
     *
     * @since 3.0.0
     *
     * @see wp_login_form()
     *
     * @param array $defaults An array of default login form arguments.
     */
    $args = wp_parse_args( $args, apply_filters( 'login_form_defaults', $defaults ) );
 
    /**
     * Filters content to display at the top of the login form.
     *
     * The filter evaluates just following the opening form tag element.
     *
     * @since 3.0.0
     *
     * @param string $content Content to display. Default empty.
     * @param array  $args    Array of login form arguments.
     */
    $login_form_top = apply_filters( 'login_form_top', '', $args );
 
    /**
     * Filters content to display in the middle of the login form.
     *
     * The filter evaluates just following the location where the 'login-password'
     * field is displayed.
     *
     * @since 3.0.0
     *
     * @param string $content Content to display. Default empty.
     * @param array  $args    Array of login form arguments.
     */
    $login_form_middle = apply_filters( 'login_form_middle', '', $args );
 
    /**
     * Filters content to display at the bottom of the login form.
     *
     * The filter evaluates just preceding the closing form tag element.
     *
     * @since 3.0.0
     *
     * @param string $content Content to display. Default empty.
     * @param array  $args    Array of login form arguments.
     */
    $login_form_bottom = apply_filters( 'login_form_bottom', '', $args );
 
    $form = '
        <form name="' . $args['form_id'] . '" id="' . $args['form_id'] . '" class="' . $args['form_class'] . '" action="' . esc_url( site_url( 'wp-login.php', 'login_post' ) ) . '" method="post">
            ' . $login_form_top . '
            <p class="login-username">
                <label for="' . esc_attr( $args['id_username'] ) . '">' . esc_html( $args['label_username'] ) . '</label>
                <input type="text" placeholder="Username or Email" name="log" id="' . esc_attr( $args['id_username'] ) . '" class="input" value="' . esc_attr( $args['value_username'] ) . '" size="20" />
            </p>
            <p class="login-password">
                <label for="' . esc_attr( $args['id_password'] ) . '">' . esc_html( $args['label_password'] ) . '</label>
                <input type="password" placeholder="Password" name="pwd" id="' . esc_attr( $args['id_password'] ) . '" class="input" value="" size="20" />
            </p>
            ' . $login_form_middle . '
            ' . ( $args['remember'] ? '<p class="login-remember"><label><input name="rememberme" type="checkbox" id="' . esc_attr( $args['id_remember'] ) . '" value="forever"' . ( $args['value_remember'] ? ' checked="checked"' : '' ) . ' /> ' . esc_html( $args['label_remember'] ) . '</label></p>' : '' ) . '
            <p class="login-submit">
                <input type="submit" name="wp-submit" id="' . esc_attr( $args['id_submit'] ) . '" class="button-primary submit btn" value="' . esc_attr( $args['label_log_in'] ) . '" />
                <input type="hidden" name="redirect_to" value="' . esc_url( $args['redirect'] ) . '" />
            </p>
            ' . $login_form_bottom . '
        </form>';
 
    if ( $args['echo'] )
        echo $form;
    else
        return $form;
}