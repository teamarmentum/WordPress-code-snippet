<?php

function holdfolio_after_new_reserve_request_send_notification($order_id,$session){
   
add_filter( 'wp_mail_content_type', 'holdfolio_set_html_mail_content_type' );

	global $wpdb,$current_user;
	get_currentuserinfo();
	$object =  wc_get_order( $order_id );
    $product_id = $object->post->post_parent;
    $email = get_post_meta($order_id,'_billing_email',true);
  
  
    $customer_id = get_post_meta($order_id,'_customer_user',true);
    $firstname = get_user_meta($customer_id,'first_name',true);
    $product_name = get_the_title($product_id);
		
		$user_name=$current_user->first_name.' '.$current_user->last_name;
		$to =get_bloginfo('admin_email');
		
		$subject = "New Reserve Request by {$user_name}";
		$amount = number_format($session['invest']['inv_amt'],2);
		
		$mail_body="<p>Dear Admin,</p>";
		$mail_body.= "New Reserve Request by {$user_name}";
		$mail_body.="<p><strong>User Email <strong>: {$current_user->user_email}</p>";
		$mail_body.="<p><strong>Amount <strong>: &#36;{$amount}</p>";
		
		//$mail_body.="<h4>Reserve Details</h4>";
	//	$mail_body.="<p></p>";
		$message = $mail_body;
		//$headers  = 'MIME-Version: 1.0' . "\r\n";
		//$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		//$headers .= 'From: Holdfolio <info@holdfolio.com>' . "\r\n";
		//$flag = mail($to, $subject, $message, $headers,'-finfo@holdfolio.com');
		
// to admin		
wp_mail( $to, $subject, $message );




    $message2 = "Hi {$firstname},<br />

We have received your reservation for {$product_name}. We will keep you up to

date as we move closer to making this investment available.<br /><br />

Sincerely,<br />

The Holdfolio Team";
// to user

$subject = ot('reserve_request_subject');
		$subject = str_replace('{firstname}',$firstname,$subject);
		$subject = str_replace('{product_name}',$product_name,$subject);
		
		
		$content = ot('reserve_request_content');
		$content = str_replace('{firstname}',$firstname,$content);
		$content = str_replace('{product_name}',$product_name,$content);
		
wp_mail( $current_user->user_email, $subject, $content );


		

remove_filter( 'wp_mail_content_type', 'holdfolio_set_html_mail_content_type' );
}
add_action('holdfolio_after_new_reserve_request','holdfolio_after_new_reserve_request_send_notification',10,2);